package ru.maximoff.service;

import ru.maximoff.domain.Disc;

public interface DiscService extends AbstractService<Disc> {
}
