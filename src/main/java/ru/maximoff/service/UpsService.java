package ru.maximoff.service;

import ru.maximoff.domain.UPS;

public interface UpsService extends AbstractService<UPS> {
    UPS loadByLockerId(Long id);
}
