package ru.maximoff.service;

import ru.maximoff.domain.Server;

import java.util.List;

public interface ServerService extends AbstractService<Server> {
    List<Server> loadServersByLockerId(Long id);
}
