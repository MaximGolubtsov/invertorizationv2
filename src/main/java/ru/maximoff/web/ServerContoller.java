package ru.maximoff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maximoff.domain.Server;
import ru.maximoff.service.ServerService;

import java.util.List;

@Controller
public class ServerContoller {

    @Autowired
    private ServerService serverService;

    @RequestMapping("/servers")
    public String list(ModelMap model){
        List<Server> servers = serverService.getAll();
        model.addAttribute("servers", servers);
        return "servers/list";
    }

    @RequestMapping("/newServer")
    public String newServer (ModelMap model){
        model.addAttribute("server", new Server());
        return "servers/newServer";
    }

    @RequestMapping (value = "/newServer", method = RequestMethod.POST)
    public String addLocker(ModelMap model, @ModelAttribute("server") Server server, BindingResult result) {
        if (result.hasErrors()){
            return newServer(model);
        }
        serverService.add(server);
        return "redirect:/servers";
    }



}
