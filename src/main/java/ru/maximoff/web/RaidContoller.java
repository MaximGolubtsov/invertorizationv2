package ru.maximoff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maximoff.domain.RAID;
import ru.maximoff.service.RaidService;

import java.util.List;

@Controller
public class RaidContoller {

    @Autowired
    private RaidService raidService;

    @RequestMapping("/raids")
    public String list(ModelMap model){
        List<RAID> raids = raidService.getAll();
        model.addAttribute("raids", raids);
        return "raids/list";
    }

    @RequestMapping("/newRaid")
    public String newRaid(ModelMap model) {
        model.addAttribute("raid", new RAID());
        return "raids/newRaid";
    }

    @RequestMapping(value = "/newRaid", method = RequestMethod.POST)
    public String addRaid(ModelMap model, @ModelAttribute("raid") RAID raid, BindingResult result) {
        if (result.hasErrors())
        {
            return newRaid(model);
        }
        raidService.add(raid);
        return "redirect/raids";
    }

}
