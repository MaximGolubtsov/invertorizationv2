package ru.maximoff.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "LOCKER")
public class Locker {

    private Long id;
    private String model;
    private String inventoryNumber;
    private String location;
    private Set<Server> servers;
    private Set<RAID> raidSet;
    private UPS ups;

    public Locker() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "MODEL")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Column(name = "INVENTORY_NUMBER")
    public String getInventoryNumber() {
        return inventoryNumber;
    }

    public void setInventoryNumber(String inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    @Column(name = "LOCATION")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @OneToMany(mappedBy = "locker", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Server> getServers() {
        return servers;
    }

    public void setServers(Set<Server> servers) {
        this.servers = servers;
    }

    @OneToMany(mappedBy = "locker", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<RAID> getRaidSet() {
        return raidSet;
    }

    public void setRaidSet(Set<RAID> raidSet) {
        this.raidSet = raidSet;
    }

    @OneToOne
    public UPS getUPS() {
        return ups;
    }

    public void setUPS(UPS ups) {
        this.ups = ups;
    }

    public void addServer(Server server) {
        server.setLocker(this);
        getServers().add(server);
    }

    public void deleteServer(Server server) {
        getServers().remove(server);
    }

    public void addRAID(RAID raid) {
        raid.setLocker(this);
        getRaidSet().add(raid);
    }

    public void deleteRAID(RAID raid) {
        getRaidSet().remove(raid);
    }
}
