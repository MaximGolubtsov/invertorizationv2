package ru.maximoff.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "RAID")
public class RAID {

    private Long id;
    private Set<Disc> discs;
    private Locker locker;

    public RAID() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "raid", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<Disc> getDiscs() {
        return discs;
    }

    public void setDiscs(Set<Disc> discs) {
        this.discs = discs;
    }

    @ManyToOne
    @JoinColumn(name = "LOCKER_ID")
    public Locker getLocker() {
        return locker;
    }

    public void setLocker(Locker locker) {
        this.locker = locker;
    }

    public void addDisc(Disc disc) {
        disc.setRaid(this);
        getDiscs().add(disc);
    }

    public void deleteDisc(Disc disc) {
        getDiscs().remove(disc);
    }
}
