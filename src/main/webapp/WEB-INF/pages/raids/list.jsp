%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list of raids</title>
</head>
<body>
<div>
    <jsp:directive.page contentType="text/html;charset=UTF-8"/>
    <h1>Список дисковых массивов</h1>
    <c:if test="${not empty raids}">
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>raid</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${raids}" var="raid">
                <tr>
                    <td><a href="raids/${raid.id}">${raid.id}</a></td>
                    <td>${raid.locker}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>

<div>
    <a href="/serverInventory/newRaid">Добавить дисковый массив</a>
</div>

</body>
</html>
