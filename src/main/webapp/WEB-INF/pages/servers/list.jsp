<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list of lockers</title>
</head>
<body>
<div>
    <jsp:directive.page contentType="text/html;charset=UTF-8"/>
    <h1>Список серверов</h1>
    <c:if test="${not empty servers}">
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>Model</th>
                <th>LockerID</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${servers}" var="server">
                <tr>
                    <td>${server.id}</td>
                    <td><a href="servers/${server.id}">${server.model}</a></td>
                    <td>${server.locker}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>

<div>
    <a href="/serverInventory/newServer">Добавить сервер</a>
</div>


</body>
</html>