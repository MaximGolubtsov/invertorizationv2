<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>list of lockers</title>
    </head>
    <body>
        <div>
            <jsp:directive.page contentType="text/html;charset=UTF-8"/>
            <h1>Список серверных шкафов</h1>
            <c:if test="${not empty lockers}">
                <table>
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Inventory Number</th>
                        <th>Model</th>
                        <th>Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${lockers}" var="locker">
                        <tr>
                            <td>${locker.id}</td>
                            <td><a href="lockers/${locker.id}">${locker.inventoryNumber}</a></td>
                            <td>${locker.model}</td>
                            <td>${locker.location}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>

    <div>
        <a href="/serverInventory/newLocker">Добавить серверный шкаф</a>
    </div>


    </body>
    </html>

