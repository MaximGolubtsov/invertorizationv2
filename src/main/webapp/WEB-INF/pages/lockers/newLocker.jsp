<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ page contentType="text/html;charset=utf-8" %>

<html>
<head>
    <title></title>
</head>
<body>
add Locker

<form:form commandName="locker" method="post" action="newLocker">


    <form:label path="inventoryNumber">Inventory Number
    </form:label>
    <form:input path="inventoryNumber" id="inventoryNumber"/>
    <p/>

    <form:label path="location">Location
    </form:label>
    <form:input path="location" id="location"/>
    <p/>

    <form:label path="model">Server Model
    </form:label>
    <form:input path="model" id="model"/>
    <p/>


    <div>
        <input type="submit" value="Добавить">
    </div>

</form:form>
</body>
</html>
